/*Összes fő kategória kilistázása*/
select * from SPORT_MAIN_CATEGORY;
/*****************************************************************************************************************/

/*összes ország kilistázása*/
select * from COUNTRY;
/*****************************************************************************************************************/

/*Megkapjuk az összes ligát*/
select * from legaues l;
/*****************************************************************************************************************/

/*megkapjuk az összes csapatot*/
select * from team t;
/*****************************************************************************************************************/

/*összes sportesemény kilistázása*/
select * from sport_event;
/*****************************************************************************************************************/

/*összes bar kilistázása*/
select * from bars;
/*****************************************************************************************************************/

/* megkapjuk melyik barokban nézhető az 1-es ID-jú event: (Bajnokok Ligája	FC Barcelona	Juventus	2020-10-28 21:00:00) */
select bars.*
from bars join event_bars on bars.pk_bar = event_bars.bar_id
where event_bars.event_id = 1;
/*****************************************************************************************************************/

/* megkapjuk melyik eventek vannak a barban, melynek ID-ja 1*/
select se.* from sport_event se
 join event_bars on se.pk_event = event_bars.event_id
where event_bars.bar_id = 1;
/*****************************************************************************************************************/