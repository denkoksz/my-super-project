DROP DATABASE IF EXISTS where_to_watch;
CREATE DATABASE where_to_watch;
USE where_to_watch;

CREATE TABLE SPORT_MAIN_CATEGORY (
 pk_main_category INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
 category_name VARCHAR(45) DEFAULT NULL,
 created_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
 updated_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

INSERT INTO sport_main_category (pk_main_category, category_name) VALUES (1, 'Labdarúgás');
INSERT INTO sport_main_category (pk_main_category, category_name) VALUES (2, 'Kézilabda');
INSERT INTO sport_main_category (pk_main_category, category_name)  VALUES (3, 'Jéghoki');
INSERT INTO sport_main_category (pk_main_category, category_name)  VALUES (4, 'Darts');


CREATE TABLE COUNTRY (
 pk_country INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
 country_name VARCHAR(50) DEFAULT NULL,
 created_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
 updated_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

INSERT INTO country (pk_country, country_name)  VALUES (1, 'Magyarország');
INSERT INTO country (pk_country, country_name) VALUES (2, 'Spanyolország');
INSERT INTO country  (pk_country, country_name) VALUES (3, 'Olaszország');
INSERT INTO country  (pk_country, country_name) VALUES (4, 'Nemzetközi');

CREATE TABLE LEAGUES (
 pk_league INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
 main_category VARCHAR(20) NOT NULL,
 country VARCHAR(30)  NOT NULL,
 league_name VARCHAR(50) default null,
 created_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
 updated_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

/* id, klub/válogatott/egyéni, foci/kézi/hoki/darts,  MO/SPAIN/ITA, name   */
INSERT INTO leagues (pk_league, main_category, country, league_name) VALUES (1, 'labdarúgás', 'Magyarország', 'NB1');
INSERT INTO leagues  (pk_league, main_category, country, league_name) VALUES (2, 'labdarúgás', 'Spanyolország', 'LaLiga');
INSERT INTO leagues (pk_league, main_category, country, league_name)  VALUES (3, 'labdarúgás', 'Nemzetközi', 'EB-Selejtező');
INSERT INTO leagues (pk_league, main_category, country, league_name)  VALUES (4, 'darts', 'Nemzetközi', 'Darts Világbajnokság');
INSERT INTO leagues (pk_league, main_category, country, league_name)  VALUES (5, 'labdarúgás', 'Olaszország', 'Seria A');
INSERT INTO leagues  (pk_league, main_category, country, league_name)  VALUES (6, 'labdarúgás', 'Nemzetközi', 'Bajnokok Ligája');

CREATE TABLE TEAM (
  pk_team INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  main_category VARCHAR(20) NOT NULL,
  country VARCHAR(30)  NOT NULL,
  league_name VARCHAR(50) default null,
  team_name VARCHAR(50) default null,
  created_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  updated_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

INSERT INTO TEAM (pk_team, main_category, country, league_name, team_name) VALUES(1, 'labdarúgás', 'Spanyolország', 'LaLiga', 'FC Barcelona');
INSERT INTO TEAM (pk_team, main_category, country, league_name, team_name) VALUES(2, 'labdarúgás', 'Olaszország', 'Serie A', 'Juventus');
INSERT INTO TEAM (pk_team, main_category, country, team_name) VALUES(3, 'labdarúgás', 'Nemzetközi',  'Magyarország');
INSERT INTO TEAM (pk_team, main_category, country, team_name) VALUES(4, 'labdarúgás', 'Nemzetközi',  'Izland');

CREATE TABLE SPORT_EVENT (
 pk_event INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
 main_category VARCHAR(20) NOT NULL,
 country VARCHAR(30)  NOT NULL,
 league_name VARCHAR(50) default null,
 home_team_name VARCHAR(50) default null,
 away_team_name VARCHAR(50) default null,
 event_date datetime NOT NULL,
 created_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
 updated_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

INSERT INTO SPORT_EVENT (pk_event, main_category, country, league_name, home_team_name, away_team_name, event_date)
VALUES (1, 'labdarúgás', 'Nemzetközi', 'Bajnokok Ligája', 'FC Barcelona', 'Juventus FC',  '2020-10-28 21:00:00');
INSERT INTO SPORT_EVENT (pk_event, main_category, country, league_name, home_team_name, away_team_name, event_date)
VALUES (2, 'labdarúgás', 'Nemzetközi', 'EB-Selejtező', 'Magyarország', 'Izland',  '2020-11-12 20:45:00');


CREATE TABLE BARS (
  pk_bar INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  country VARCHAR (50) NOT NULL,
  name VARCHAR (150) NOT NULL,
  zip_code VARCHAR(10) NOT NULL,
  city VARCHAR(30) NOT NULL,
  street VARCHAR (100) NOT NULL,
  house_number VARCHAR(10),
  phone VARCHAR(50),
  email VARCHAR(100),
  credit_card boolean,
  szep_card boolean,
  bar_link VARCHAR(255),
  created_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  updated_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP
)ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

INSERT INTO BARS (pk_bar, country, name, zip_code, city, street, house_number, phone, email, credit_card, szep_card, bar_link)
VALUES(1, 'Magyarország', 'Denkoksz Bar', '1119', 'Budapest', 'Zámori utca', '18', '0036202997855', 'denkoksz@gmail.com', true, true, 'http://www.denkokszbar.com');
INSERT INTO BARS (pk_bar, country, name, zip_code, city, street, house_number, phone, email, credit_card, szep_card, bar_link)
VALUES(2, 'Magyarország', 'Kata Bar', '1032', 'Budapest', 'Szőlő köz', '8', '0036301234567', 'tinka1811@gmail.com', true, false, 'http://www.katabar.com');

CREATE TABLE EVENT_BARS (
    bar_id INT NOT NULL,
    event_id INT NOT NULL,
    created_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,
    updated_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (bar_id, event_id),
    CONSTRAINT `constr_event_bars_bar_fk`
        FOREIGN KEY `bars_fk` (`bar_id`) REFERENCES `BARS` (`pk_bar`),
    CONSTRAINT `constr_event_bars_event_fk`
        FOREIGN KEY `event_fk` (`event_id`) REFERENCES `SPORT_EVENT` (`pk_event`)
)ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

INSERT INTO EVENT_BARS (bar_id, event_id) VALUES (1, 1);
INSERT INTO EVENT_BARS  (bar_id, event_id) VALUES (1, 2);
INSERT INTO EVENT_BARS  (bar_id, event_id) VALUES (2, 1);
INSERT INTO EVENT_BARS  (bar_id, event_id) VALUES (2, 2);