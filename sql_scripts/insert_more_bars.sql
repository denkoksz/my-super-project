INSERT INTO BARS (pk_bar, country, name, zip_code, city, street, house_number, phone, email, credit_card, szep_card, bar_link)
VALUES(3, 'Magyarország', 'Budapest Park', '1097', 'Budapest', 'Park utca', '18', '0036202993654', 'budapestpark@gmail.com', true, true, 'http://www.budapestpark.com');

INSERT INTO BARS (pk_bar, country, name, zip_code, city, street, house_number, phone, email, credit_card, szep_card, bar_link)
VALUES(4, 'Magyarország', 'Ördög Sarok', '1365', 'Budapest', 'Ördug utca', '666', '066696996669', 'ordogsarok@gmail.com', true, true, 'http://www.ordogsarok.com');

INSERT INTO BARS (pk_bar, country, name, zip_code, city, street, house_number, phone, email, credit_card, szep_card, bar_link)
VALUES(5, 'Magyarország', 'Bohém Söröző', '1078', 'Budapest', 'Bohém utca', '65', '003620245874', 'bohem@gmail.com', true, true, 'http://www.bohem.com');

INSERT INTO BARS (pk_bar, country, name, zip_code, city, street, house_number, phone, email, credit_card, szep_card, bar_link)
VALUES(6, 'Magyarország', 'KRAFT', '9874', 'Budapest', 'Kraft utca', '12', '0036202996314', 'kraft@gmail.com', true, true, 'http://www.kraft.com');

INSERT INTO BARS (pk_bar, country, name, zip_code, city, street, house_number, phone, email, credit_card, szep_card, bar_link)
VALUES(7, 'Magyarország', 'Parlament', '1205', 'Budapest', 'Széchenyi Tér', '1', '00362026524', 'parlament@gmail.com', true, true, 'http://www.parlament.com');

INSERT INTO BARS (pk_bar, country, name, zip_code, city, street, house_number, phone, email, credit_card, szep_card, bar_link)
VALUES(8, 'Magyarország', 'Emarsys', '1032', 'Budapest', 'Kossuth Lajos utca', '15', '0036202993587', 'emarsys@gmail.com', true, true, 'http://www.emarsys.com');

INSERT INTO BARS (pk_bar, country, name, zip_code, city, street, house_number, phone, email, credit_card, szep_card, bar_link)
VALUES(9, 'Magyarország', 'Zero Bar', '1865', 'Budapest', 'Zero utca', '2', '0036205473654', 'zero@gmail.com', true, true, 'http://www.zero.com');

INSERT INTO BARS (pk_bar, country, name, zip_code, city, street, house_number, phone, email, credit_card, szep_card, bar_link)
VALUES(10, 'Magyarország', '4es 6os Söröző', '1416', 'Budapest', 'Teréz Körút', '46', '0036204646464', '46@gmail.com', true, true, 'http://www.46sorozo.com');