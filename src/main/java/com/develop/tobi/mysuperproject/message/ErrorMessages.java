package com.develop.tobi.mysuperproject.message;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

public class ErrorMessages {

  public static final ServiceMessage UNKNOWN =
    new ServiceMessage("E0001", INTERNAL_SERVER_ERROR, "Internal Server Error, check back later or contact support");
  public static final ServiceMessage STATE_CHANGED_REFRESH_REQUIRED =
    new ServiceMessage("E0002", "The state is no more valid, a refresh is required");
  public static final ServiceMessage BAR_NOT_FOUND =
    new ServiceMessage("E0003", "The state is no more valid, a refresh is required");
}
