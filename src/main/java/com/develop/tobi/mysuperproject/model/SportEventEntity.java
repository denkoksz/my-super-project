package com.develop.tobi.mysuperproject.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = EntityConsts.SPORT_EVENTS_TABLE_NAME, schema = EntityConsts.SCHEMA_NAME)
public class SportEventEntity extends AbstractPersistable<SportEventEntity> implements Serializable {

  private int pkEvent;
  private String mainCategory;
  private String country;
  private String leagueName;
  private String homeTeamName;
  private String awayTeamName;
  private LocalDateTime eventDate;

  public SportEventEntity() {
  }

  @Id
  @Column(name = "pk_event")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  public int getPkEvent() {
    return pkEvent;
  }

  public void setPkEvent(final int pkEvent) {
    this.pkEvent = pkEvent;
  }

  @Basic
  @Column(name = "main_category")
  public String getMainCategory() {
    return mainCategory;
  }

  public void setMainCategory(final String mainCategory) {
    this.mainCategory = mainCategory;
  }

  @Basic
  @Column(name = "country")
  public String getCountry() {
    return country;
  }

  public void setCountry(final String country) {
    this.country = country;
  }

  @Basic
  @Column(name = "league_name")
  public String getLeagueName() {
    return leagueName;
  }

  public void setLeagueName(final String leagueName) {
    this.leagueName = leagueName;
  }

  @Basic
  @Column(name = "home_team_name")
  public String getHomeTeamName() {
    return homeTeamName;
  }

  public void setHomeTeamName(final String homeTeamName) {
    this.homeTeamName = homeTeamName;
  }

  @Basic
  @Column(name = "away_team_name")
  public String getAwayTeamName() {
    return awayTeamName;
  }

  public void setAwayTeamName(final String awayTeamName) {
    this.awayTeamName = awayTeamName;
  }

  @Basic
  @Column(name = "event_date")
  public LocalDateTime getEventDate() {
    return eventDate;
  }

  public void setEventDate(final LocalDateTime eventDate) {
    this.eventDate = eventDate;
  }

  public SportEventEntity(final String mainCategory, final String country,
                          final String leagueName, final String homeTeamName, final String awayTeamName, final LocalDateTime eventDate) {
    this.mainCategory = mainCategory;
    this.country = country;
    this.leagueName = leagueName;
    this.homeTeamName = homeTeamName;
    this.awayTeamName = awayTeamName;
    this.eventDate = eventDate;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    final SportEventEntity that = (SportEventEntity) o;
    return pkEvent == that.pkEvent &&
      Objects.equals(mainCategory, that.mainCategory) &&
      Objects.equals(country, that.country) &&
      Objects.equals(leagueName, that.leagueName) &&
      Objects.equals(homeTeamName, that.homeTeamName) &&
      Objects.equals(awayTeamName, that.awayTeamName) &&
      Objects.equals(eventDate, that.eventDate);
  }

  @Override
  public int hashCode() {
    return Objects.hash(pkEvent, mainCategory, country, leagueName, homeTeamName, awayTeamName, eventDate);
  }

  @Override
  public String toString() {
    return "SportEventEntity{" +
      "pkEvent=" + pkEvent +
      ", mainCategory='" + mainCategory + '\'' +
      ", country='" + country + '\'' +
      ", leagueName='" + leagueName + '\'' +
      ", homeTeamName='" + homeTeamName + '\'' +
      ", awayTeamName='" + awayTeamName + '\'' +
      ", eventDate=" + eventDate +
      '}';
  }
}
