package com.develop.tobi.mysuperproject.model;

import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = EntityConsts.LEAGUES_TABLE_NAME, schema = EntityConsts.SCHEMA_NAME)
public class LeaguesEntity extends AbstractPersistable<LeaguesEntity> {

  private int pkLeague;
  private String mainCategory;
  private String country;
  private String leagueName;

  @Id
  @Column(name = "pk_league")
  public int getPkLeague() {
    return pkLeague;
  }

  public void setPkLeague(final int pkLeague) {
    this.pkLeague = pkLeague;
  }

  @Basic
  @Column(name = "main_category")
  public String getMainCategory() {
    return mainCategory;
  }

  public void setMainCategory(final String mainCategory) {
    this.mainCategory = mainCategory;
  }

  @Basic
  @Column(name = "country")
  public String getCountry() {
    return country;
  }

  public void setCountry(final String country) {
    this.country = country;
  }

  @Basic
  @Column(name = "league_name")
  public String getLeagueName() {
    return leagueName;
  }

  public void setLeagueName(final String leagueName) {
    this.leagueName = leagueName;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    final LeaguesEntity that = (LeaguesEntity) o;
    return pkLeague == that.pkLeague &&
      Objects.equals(mainCategory, that.mainCategory) &&
      Objects.equals(country, that.country) &&
      Objects.equals(leagueName, that.leagueName);
  }

  @Override
  public int hashCode() {
    return Objects.hash(pkLeague, mainCategory, country, leagueName);
  }
}
