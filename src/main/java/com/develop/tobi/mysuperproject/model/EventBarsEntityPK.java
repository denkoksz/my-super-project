package com.develop.tobi.mysuperproject.model;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Id;

public class EventBarsEntityPK implements Serializable {

  private int barId;
  private int eventId;

  @Column(name = "bar_id")
  @Id
  public int getBarId() {
    return barId;
  }

  public void setBarId(final int barId) {
    this.barId = barId;
  }

  @Column(name = "event_id")
  @Id
  public int getEventId() {
    return eventId;
  }

  public void setEventId(final int eventId) {
    this.eventId = eventId;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    final EventBarsEntityPK that = (EventBarsEntityPK) o;
    return barId == that.barId &&
      eventId == that.eventId;
  }

  @Override
  public int hashCode() {
    return Objects.hash(barId, eventId);
  }
}
