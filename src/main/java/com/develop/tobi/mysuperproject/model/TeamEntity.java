package com.develop.tobi.mysuperproject.model;

import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = EntityConsts.TEAM_TABLE_NAME, schema = EntityConsts.SCHEMA_NAME)
public class TeamEntity extends AbstractPersistable<TeamEntity> {

  private int pkTeam;
  private String mainCategory;
  private String country;
  private String leagueName;
  private String teamName;

  @Id
  @Column(name = "pk_team")
  public int getPkTeam() {
    return pkTeam;
  }

  public void setPkTeam(final int pkTeam) {
    this.pkTeam = pkTeam;
  }

  @Basic
  @Column(name = "main_category")
  public String getMainCategory() {
    return mainCategory;
  }

  public void setMainCategory(final String mainCategory) {
    this.mainCategory = mainCategory;
  }

  @Basic
  @Column(name = "country")
  public String getCountry() {
    return country;
  }

  public void setCountry(final String country) {
    this.country = country;
  }

  @Basic
  @Column(name = "league_name")
  public String getLeagueName() {
    return leagueName;
  }

  public void setLeagueName(final String legaueName) {
    this.leagueName = legaueName;
  }

  @Basic
  @Column(name = "team_name")
  public String getTeamName() {
    return teamName;
  }

  public void setTeamName(final String teamName) {
    this.teamName = teamName;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    final TeamEntity that = (TeamEntity) o;
    return pkTeam == that.pkTeam &&
      Objects.equals(mainCategory, that.mainCategory) &&
      Objects.equals(country, that.country) &&
      Objects.equals(leagueName, that.leagueName) &&
      Objects.equals(teamName, that.teamName);
  }

  @Override
  public int hashCode() {
    return Objects.hash(pkTeam, mainCategory, country, leagueName, teamName);
  }
}
