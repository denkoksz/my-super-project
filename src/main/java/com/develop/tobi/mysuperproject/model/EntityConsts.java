package com.develop.tobi.mysuperproject.model;

public class EntityConsts {

  public static final String CREATED_TIME = "created_time";
  public static final String ID = "id";
  public static final String UPDATED_TIME = "updated_time";

  public static final String SCHEMA_NAME = "WHERE_TO_WATCH";

  public static final String BARS_TABLE_NAME = "bars";
  public static final String COUNTRY_TABLE_NAME = "country";
  public static final String EVENT_BARS_TABLE_NAME = "event_bars";
  public static final String LEAGUES_TABLE_NAME = "leagues";
  public static final String SPORT_EVENTS_TABLE_NAME = "sport_event";
  public static final String SPORT_MAIN_CATEGORY_TABLE_NAME = "sport_main_category";
  public static final String TEAM_TABLE_NAME = "team";

}
