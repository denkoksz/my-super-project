package com.develop.tobi.mysuperproject.model;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = EntityConsts.EVENT_BARS_TABLE_NAME, schema = EntityConsts.SCHEMA_NAME)
@IdClass(EventBarsEntityPK.class)
public class EventBarsEntity extends AbstractPersistable<EventBarsEntity> implements Serializable {

  private int barId;
  private int eventId;

  public EventBarsEntity() {
  }

  public EventBarsEntity(final int barId, final int eventId) {
    this.barId = barId;
    this.eventId = eventId;
  }

  @Id
  @Column(name = "bar_id")
  public int getBarId() {
    return barId;
  }

  public void setBarId(final int barId) {
    this.barId = barId;
  }

  @Id
  @Column(name = "event_id")
  public int getEventId() {
    return eventId;
  }

  public void setEventId(final int eventId) {
    this.eventId = eventId;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    final EventBarsEntity that = (EventBarsEntity) o;
    return barId == that.barId &&
      eventId == that.eventId;
  }

  @Override
  public int hashCode() {
    return Objects.hash(barId, eventId);
  }

  @Override
  public String toString() {
    return "EventBarsEntity{" +
      "barId=" + barId +
      ", eventId=" + eventId +
      '}';
  }
}
