package com.develop.tobi.mysuperproject.model;

import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = EntityConsts.COUNTRY_TABLE_NAME, schema = EntityConsts.SCHEMA_NAME)
public class CountryEntity extends AbstractPersistable<CountryEntity> {

  private int pkCountry;
  private String countryName;

  @Id
  @Column(name = "pk_country")
  public int getPkCountry() {
    return pkCountry;
  }

  public void setPkCountry(final int pkCountry) {
    this.pkCountry = pkCountry;
  }

  @Basic
  @Column(name = "country_name")
  public String getCountryName() {
    return countryName;
  }

  public void setCountryName(final String countryName) {
    this.countryName = countryName;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    final CountryEntity that = (CountryEntity) o;
    return pkCountry == that.pkCountry &&
      Objects.equals(countryName, that.countryName);
  }

  @Override
  public int hashCode() {
    return Objects.hash(pkCountry, countryName);
  }
}
