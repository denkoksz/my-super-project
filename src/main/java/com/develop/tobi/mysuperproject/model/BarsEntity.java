package com.develop.tobi.mysuperproject.model;

import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = EntityConsts.BARS_TABLE_NAME, schema = EntityConsts.SCHEMA_NAME)
public class BarsEntity extends AbstractPersistable<BarsEntity> {

  private int pkBar;
  private String country;
  private String name;
  private String zipCode;
  private String city;
  private String street;
  private String houseNumber;
  private String phone;
  private String email;
  private Integer creditCard;
  private Integer szepCard;
  private String barLink;

  public BarsEntity() {
  }

  public BarsEntity(final String country, final String name, final String zipCode, final String city,
                    final String street, final String houseNumber, final String phone, final String email,
                    final Integer creditCard, final Integer szepCard, final String barLink)
  {
    this.country = country;
    this.name = name;
    this.zipCode = zipCode;
    this.city = city;
    this.street = street;
    this.houseNumber = houseNumber;
    this.phone = phone;
    this.email = email;
    this.creditCard = creditCard;
    this.szepCard = szepCard;
    this.barLink = barLink;
  }

  @Id
  @Column(name = "pk_bar")
  public int getPkBar() {
    return pkBar;
  }

  public void setPkBar(final int pkBar) {
    this.pkBar = pkBar;
  }

  @Basic
  @Column(name = "country")
  public String getCountry() {
    return country;
  }

  public void setCountry(final String country) {
    this.country = country;
  }

  @Basic
  @Column(name = "name")
  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  @Basic
  @Column(name = "zip_code")
  public String getZipCode() {
    return zipCode;
  }

  public void setZipCode(final String zipCode) {
    this.zipCode = zipCode;
  }

  @Basic
  @Column(name = "city")
  public String getCity() {
    return city;
  }

  public void setCity(final String city) {
    this.city = city;
  }

  @Basic
  @Column(name = "street")
  public String getStreet() {
    return street;
  }

  public void setStreet(final String street) {
    this.street = street;
  }

  @Basic
  @Column(name = "house_number")
  public String getHouseNumber() {
    return houseNumber;
  }

  public void setHouseNumber(final String houseNumber) {
    this.houseNumber = houseNumber;
  }

  @Basic
  @Column(name = "phone")
  public String getPhone() {
    return phone;
  }

  public void setPhone(final String phone) {
    this.phone = phone;
  }

  @Basic
  @Column(name = "email")
  public String getEmail() {
    return email;
  }

  public void setEmail(final String email) {
    this.email = email;
  }

  @Basic
  @Column(name = "credit_card")
  public Integer getCreditCard() {
    return creditCard;
  }

  public void setCreditCard(final Integer creditCard) {
    this.creditCard = creditCard;
  }

  @Basic
  @Column(name = "szep_card")
  public Integer getSzepCard() {
    return szepCard;
  }

  public void setSzepCard(final Integer szepCard) {
    this.szepCard = szepCard;
  }

  @Basic
  @Column(name = "bar_link")
  public String getBarLink() {
    return barLink;
  }

  public void setBarLink(final String barLink) {
    this.barLink = barLink;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    final BarsEntity that = (BarsEntity) o;
    return pkBar == that.pkBar &&
      Objects.equals(country, that.country) &&
      Objects.equals(name, that.name) &&
      Objects.equals(zipCode, that.zipCode) &&
      Objects.equals(city, that.city) &&
      Objects.equals(street, that.street) &&
      Objects.equals(houseNumber, that.houseNumber) &&
      Objects.equals(phone, that.phone) &&
      Objects.equals(email, that.email) &&
      Objects.equals(creditCard, that.creditCard) &&
      Objects.equals(szepCard, that.szepCard) &&
      Objects.equals(barLink, that.barLink);
  }

  @Override
  public int hashCode() {
    return Objects.hash(pkBar, country, name, zipCode, city, street, houseNumber, phone, email, creditCard, szepCard, barLink);
  }
}
