package com.develop.tobi.mysuperproject.model;

import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = EntityConsts.SPORT_MAIN_CATEGORY_TABLE_NAME, schema = EntityConsts.SCHEMA_NAME)
public class SportMainCategoryEntity extends AbstractPersistable<SportMainCategoryEntity> {

  private int pkMainCategory;
  private String categoryName;

  @Id
  @Column(name = "pk_main_category")
  public int getPkMainCategory() {
    return pkMainCategory;
  }

  public void setPkMainCategory(final int pkMainCategory) {
    this.pkMainCategory = pkMainCategory;
  }

  @Basic
  @Column(name = "category_name")
  public String getCategoryName() {
    return categoryName;
  }

  public void setCategoryName(final String categoryName) {
    this.categoryName = categoryName;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    final SportMainCategoryEntity that = (SportMainCategoryEntity) o;
    return pkMainCategory == that.pkMainCategory &&
      Objects.equals(categoryName, that.categoryName);
  }

  @Override
  public int hashCode() {
    return Objects.hash(pkMainCategory, categoryName);
  }
}
