package com.develop.tobi.mysuperproject.model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Objects;

import static com.develop.tobi.mysuperproject.model.EntityConsts.CREATED_TIME;
import static com.develop.tobi.mysuperproject.model.EntityConsts.UPDATED_TIME;

@MappedSuperclass
public abstract class AbstractPersistable<T extends AbstractPersistable<T>> {


  @Temporal(TemporalType.TIMESTAMP)
  @Column(nullable = false, name = CREATED_TIME)
  private Timestamp createdTime;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(nullable = false, name = UPDATED_TIME)
  private Timestamp updatedTime;

  @PrePersist
  protected void onCreate() {
    createdTime = Timestamp.valueOf(LocalDateTime.now());
    onUpdate();
  }

  @PreUpdate
  protected void onUpdate() {
    updatedTime = Timestamp.valueOf(LocalDateTime.now());
  }

  public Date getUpdatedTime() {
    return updatedTime;
  }

  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(final Timestamp createdTime) {
    this.createdTime = createdTime;
  }

  public void setUpdatedTime(final Timestamp updatedTime) {
    this.updatedTime = updatedTime;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    AbstractPersistable<?> that = (AbstractPersistable<?>) o;
    return Objects.equals(createdTime, that.createdTime) && Objects.equals(updatedTime, that.updatedTime);
  }

  @Override
  public int hashCode() {
    return Objects.hash(createdTime, updatedTime);
  }
}
