package com.develop.tobi.mysuperproject.bars.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.develop.tobi.mysuperproject.bars.repository.BarsRepository;
import com.develop.tobi.mysuperproject.model.BarsEntity;

@Service
public class BarsServiceImpl implements BarsService {

  private final BarsRepository barsRepository;

  @Autowired
  public BarsServiceImpl(final BarsRepository barsRepository) {
    this.barsRepository = barsRepository;
  }

  @Override
  public List<BarsEntity> findAll() {
    return barsRepository.findAll();
  }

  @Override
  public BarsEntity findById(final Integer barId) {
    return barsRepository.findById(barId).orElseThrow(() -> new RuntimeException("Bar not found with ID: " + barId));
  }

  @Override
  public BarsEntity save(BarsEntity barsEntity) {
    return barsRepository.save(barsEntity);
  }

  @Override
  public void deleteBarById(Integer id) {
    barsRepository.deleteById(id);
  }
}
