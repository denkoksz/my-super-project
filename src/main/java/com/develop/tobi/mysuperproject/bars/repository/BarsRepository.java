package com.develop.tobi.mysuperproject.bars.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.develop.tobi.mysuperproject.model.BarsEntity;

@Repository
public interface BarsRepository extends JpaRepository<BarsEntity, Integer> {

}
