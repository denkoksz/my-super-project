package com.develop.tobi.mysuperproject.bars.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.develop.tobi.mysuperproject.bars.service.BarsService;
import com.develop.tobi.mysuperproject.model.BarsEntity;

@RestController
@RequestMapping("/api/bars")
public class BarsController {

  private final BarsService barsService;

  @Autowired
  public BarsController(final BarsService barsService) {
    this.barsService = barsService;
  }

  @GetMapping
  public List<BarsEntity> getAllBars() {
    return barsService.findAll();
  }

  @GetMapping(value = "/{id}")
  public BarsEntity findById(@PathVariable("id") Integer id) {
    return barsService.findById(id);
  }

  @PostMapping
  public BarsEntity createNewBar(@RequestBody BarsEntity barsEntity) {
    return barsService.save(barsEntity);
  }

  @DeleteMapping(value = "/{id}")
  public void deleteBarById(@PathVariable("id") Integer id) {
    barsService.deleteBarById(id);
  }

  @PutMapping(value = "/{id}")
  public BarsEntity updateBarById(@RequestBody BarsEntity barsEntity, @PathVariable Integer id){
    barsService.findById(id);
    barsEntity.setPkBar(id);
    return barsService.save(barsEntity);
  }
}
