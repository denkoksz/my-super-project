package com.develop.tobi.mysuperproject.bars.service;

import java.util.List;

import com.develop.tobi.mysuperproject.model.BarsEntity;

public interface BarsService {

  BarsEntity findById(Integer barId);

  BarsEntity save(BarsEntity barsEntity);

  List<BarsEntity> findAll();

  void deleteBarById(Integer id);

}
