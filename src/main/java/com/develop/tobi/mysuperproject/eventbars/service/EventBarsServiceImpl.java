package com.develop.tobi.mysuperproject.eventbars.service;

import com.develop.tobi.mysuperproject.eventbars.repository.EventBarsRepository;
import com.develop.tobi.mysuperproject.model.EventBarsEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EventBarsServiceImpl implements EventBarsService {

  private final EventBarsRepository eventBarsRepository;

  @Autowired
  public EventBarsServiceImpl(final EventBarsRepository eventBarsRepository) {
    this.eventBarsRepository = eventBarsRepository;
  }

  @Override
  public List<EventBarsEntity> findAll() {
    return eventBarsRepository.findAll();
  }
}
