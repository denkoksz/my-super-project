package com.develop.tobi.mysuperproject.eventbars.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.develop.tobi.mysuperproject.model.EventBarsEntity;

@Repository
public interface EventBarsRepository extends JpaRepository<EventBarsEntity, Integer> {

}
