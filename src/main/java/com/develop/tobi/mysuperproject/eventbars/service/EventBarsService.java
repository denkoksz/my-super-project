package com.develop.tobi.mysuperproject.eventbars.service;

import com.develop.tobi.mysuperproject.model.EventBarsEntity;

import java.util.List;

public interface EventBarsService {

  List<EventBarsEntity> findAll();

}
