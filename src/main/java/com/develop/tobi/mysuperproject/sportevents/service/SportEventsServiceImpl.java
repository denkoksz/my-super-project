package com.develop.tobi.mysuperproject.sportevents.service;

import com.develop.tobi.mysuperproject.model.BarsEntity;
import com.develop.tobi.mysuperproject.model.SportEventEntity;
import com.develop.tobi.mysuperproject.sportevents.repository.SportEventsRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SportEventsServiceImpl implements SportEventsService {

  private final SportEventsRepository sportEventsRepository;

  public SportEventsServiceImpl(final SportEventsRepository sportEventsRepository) {
    this.sportEventsRepository = sportEventsRepository;
  }

  @Override
  public List<SportEventEntity> findAll() {
    return sportEventsRepository.findAll();
  }

  @Override
  public SportEventEntity save(final SportEventEntity sportEventEntity) {
    return sportEventsRepository.save(sportEventEntity);
  }

  @Override
  public List<SportEventEntity> getSportEventsInNextXDays(Integer days) {
    return sportEventsRepository.getSportEventsInNextXDays(days);
  }

  @Override
  public List<SportEventEntity> getSportEvents() {
    return sportEventsRepository.getSportEvents();
  }

  @Override
  public List<SportEventEntity> getSportEventsByBarId(Integer barId) {
    return sportEventsRepository.getSportEventsByBarId(barId);
  }

  @Override
  public List<BarsEntity> getBarsBySportEventId(Integer eventId) {
    return sportEventsRepository.getBarsBySportEventId(eventId);
  }

  @Override
  public void createSportEvent(final SportEventEntity sportEventEntity) {
    checkIfSportEventExists(sportEventEntity);
    sportEventsRepository.createSportEvent(sportEventEntity);
  }

  @Override
  public void addSportEventForBar(final SportEventEntity sportEventEntity, final Integer barId) {
    SportEventEntity event = findSportEvent(sportEventEntity);
    if (event == null) {
      createSportEvent(sportEventEntity);
    }
    sportEventsRepository.addSportEventForBar(sportEventEntity, barId);
  }

  @Override
  public boolean checkIfSportEventExists(final SportEventEntity sportEventEntity) {

    if (sportEventsRepository.checkIfSportEventExists(sportEventEntity)) {
      throw new RuntimeException("Sport event already exists, or has been created while you were creating the event! Please check the " +
        "Sport Event List again");
    }
    return false;
  }

  @Override
  public SportEventEntity findSportEvent(SportEventEntity sportEventEntity) {
    return sportEventsRepository.findSportEvent(sportEventEntity);
  }

  @Override
  public SportEventEntity findSportEventById(Integer sportEventId) {
    return sportEventsRepository.findSportEventById(sportEventId);
  }

  @Override
  public void deleteSportEventById(Integer sportEventId) {
    sportEventsRepository.deleteById(sportEventId);
  }

}
