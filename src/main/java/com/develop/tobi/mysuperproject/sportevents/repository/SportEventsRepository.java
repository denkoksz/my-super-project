package com.develop.tobi.mysuperproject.sportevents.repository;

import com.develop.tobi.mysuperproject.model.SportEventEntity;
import com.develop.tobi.mysuperproject.sportevents.repository.custom.SportEventsCustomRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SportEventsRepository extends SportEventsCustomRepository, JpaRepository<SportEventEntity, Integer> {

}
