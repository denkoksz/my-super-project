package com.develop.tobi.mysuperproject.sportevents.repository.impl;

import com.develop.tobi.mysuperproject.model.*;
import com.develop.tobi.mysuperproject.sportevents.repository.custom.SportEventsCustomRepository;
import com.querydsl.jpa.impl.JPAQuery;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalDateTime;
import java.util.List;

public class SportEventsRepositoryImpl implements SportEventsCustomRepository {

    private static final QSportEventEntity SPORT_EVENT_ENTITY = QSportEventEntity.sportEventEntity;
    private static final QBarsEntity BARS_ENTITY = QBarsEntity.barsEntity;
    private static final QEventBarsEntity EVENT_BARS_ENTITY = QEventBarsEntity.eventBarsEntity;

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<SportEventEntity> getSportEventsInNextXDays(Integer days) {
        final JPAQuery<SportEventEntity> query = new JPAQuery<>(entityManager);

        query.from(SPORT_EVENT_ENTITY)
                .where(SPORT_EVENT_ENTITY.eventDate.between(LocalDateTime.now(), LocalDateTime.now().plusDays(days)));

        return query.select(SPORT_EVENT_ENTITY).fetch();
    }

    @Override
    public List<SportEventEntity> getSportEvents() {
        final JPAQuery<SportEventEntity> query = new JPAQuery<>(entityManager);

        query.from(SPORT_EVENT_ENTITY)
                .where(SPORT_EVENT_ENTITY.eventDate.after(LocalDateTime.now()));

        return query.select(SPORT_EVENT_ENTITY).fetch();
    }

    @Override
    public List<BarsEntity> getBarsBySportEventId(final Integer eventId) {
        final JPAQuery<BarsEntity> query = new JPAQuery<>(entityManager);
        query.from(BARS_ENTITY)
                .leftJoin(EVENT_BARS_ENTITY).on(BARS_ENTITY.pkBar.eq(EVENT_BARS_ENTITY.barId))
                .where(EVENT_BARS_ENTITY.eventId.eq(eventId));

        return query.select(BARS_ENTITY).fetch();
    }

    @Override
    public List<SportEventEntity> getSportEventsByBarId(final Integer barId) {
        final JPAQuery<SportEventEntity> query = new JPAQuery<>(entityManager);
        query.from(SPORT_EVENT_ENTITY)
                .leftJoin(EVENT_BARS_ENTITY).on(SPORT_EVENT_ENTITY.pkEvent.eq(EVENT_BARS_ENTITY.eventId))
                .where(EVENT_BARS_ENTITY.barId.eq(barId)
                        .and(SPORT_EVENT_ENTITY.eventDate.after(LocalDateTime.now())));

        return query.select(SPORT_EVENT_ENTITY).fetch();
    }

    @Override
    @Transactional
    public void addSportEventForBar(final SportEventEntity sportEventEntity, final Integer barId) {
        Integer maxPKEvent = findMaxSportEventEntityPK();
        entityManager.persist(new EventBarsEntity(barId, maxPKEvent));
    }

    @Override
    @Transactional
    public void createSportEvent(final SportEventEntity sportEventEntity) {
        entityManager.persist(sportEventEntity);
    }

    public boolean checkIfSportEventExists(final SportEventEntity sportEventEntity) {
        return findSportEvent(sportEventEntity) != null;
    }

    @Override
    public SportEventEntity findSportEvent(SportEventEntity sportEventEntity) {
        final JPAQuery<SportEventEntity> query = new JPAQuery<>(entityManager);


        query.from(SPORT_EVENT_ENTITY).
                where(SPORT_EVENT_ENTITY.homeTeamName.eq(sportEventEntity.getHomeTeamName())
                .and(SPORT_EVENT_ENTITY.awayTeamName.eq(sportEventEntity.getAwayTeamName())
                .and(SPORT_EVENT_ENTITY.mainCategory.eq(sportEventEntity.getMainCategory())
                .and(SPORT_EVENT_ENTITY.eventDate.between(sportEventEntity.getEventDate().minusDays(1), sportEventEntity.getEventDate().plusDays(1)))
                )));

        return query.select(SPORT_EVENT_ENTITY).fetchFirst();
    }

    @Override
    public SportEventEntity findSportEventById(Integer sportEventId) {
        final JPAQuery<SportEventEntity> query = new JPAQuery<>(entityManager);

        query.from(SPORT_EVENT_ENTITY).where(SPORT_EVENT_ENTITY.pkEvent.eq(sportEventId));

        return query.select(SPORT_EVENT_ENTITY).fetchFirst();
    }

    private Integer findMaxSportEventEntityPK() {
        final JPAQuery<SportEventEntity> query = new JPAQuery<>(entityManager);

        query.from(SPORT_EVENT_ENTITY).orderBy(SPORT_EVENT_ENTITY.pkEvent.desc()).limit(1);

        return query.select(SPORT_EVENT_ENTITY.pkEvent).fetchFirst();
    }
}
