package com.develop.tobi.mysuperproject.sportevents.repository.custom;

import com.develop.tobi.mysuperproject.model.BarsEntity;
import com.develop.tobi.mysuperproject.model.SportEventEntity;

import java.util.List;

public interface SportEventsCustomRepository {

  List<SportEventEntity> getSportEventsInNextXDays(Integer days);

  List<SportEventEntity> getSportEvents();

  List<BarsEntity> getBarsBySportEventId(Integer eventId);

  List<SportEventEntity> getSportEventsByBarId(Integer barId);

  void addSportEventForBar(SportEventEntity sportEventEntity, Integer barId);

  void createSportEvent(SportEventEntity sportEventEntity);

  boolean checkIfSportEventExists(SportEventEntity sportEventEntity);

  SportEventEntity findSportEvent(SportEventEntity sportEventEntity);

  SportEventEntity findSportEventById(Integer sportEventId);
}
