package com.develop.tobi.mysuperproject.sportevents.service;

import java.util.List;

import com.develop.tobi.mysuperproject.model.BarsEntity;
import com.develop.tobi.mysuperproject.model.SportEventEntity;

public interface SportEventsService {

  List<SportEventEntity> findAll();

  List<SportEventEntity> getSportEventsInNextXDays(Integer days);

  List<SportEventEntity> getSportEvents();

  List<SportEventEntity> getSportEventsByBarId(Integer barId);

  List<BarsEntity> getBarsBySportEventId(Integer eventId);

  SportEventEntity save(SportEventEntity sportEventEntity);

  void addSportEventForBar(SportEventEntity sportEventEntity, Integer barId);

  void createSportEvent(SportEventEntity sportEventEntity);

  boolean checkIfSportEventExists(SportEventEntity sportEventEntity);

  SportEventEntity findSportEvent(SportEventEntity sportEventEntity);

  SportEventEntity findSportEventById(Integer sportEventId);

  void deleteSportEventById(Integer sportEventId);
}
