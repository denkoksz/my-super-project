package com.develop.tobi.mysuperproject.sportevents.controller;

import com.develop.tobi.mysuperproject.model.BarsEntity;
import com.develop.tobi.mysuperproject.model.SportEventEntity;
import com.develop.tobi.mysuperproject.sportevents.service.SportEventsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/sportevents")
public class SportEventsController {

  private final SportEventsService sporteventsService;

  @Autowired
  public SportEventsController(final SportEventsService sporteventsService) {
    this.sporteventsService = sporteventsService;
  }

  @GetMapping
  public List<SportEventEntity> getAllSportEvents() {
    return sporteventsService.getSportEvents();
  }


  @GetMapping(value = "/incoming")
  public List<SportEventEntity> getAllSportEventInNextXDays(@RequestParam Integer days) {
    return sporteventsService.getSportEventsInNextXDays(days);
  }

  @GetMapping(value = "/bars/{id}/events")
  public List<SportEventEntity> getSportEventsByBarId(@PathVariable Integer id) {
    return sporteventsService.getSportEventsByBarId(id);
  }

  @GetMapping(value = "/{id}/bars")
  public List<BarsEntity> getBarsBySportEventId(@PathVariable Integer id) {
    return sporteventsService.getBarsBySportEventId(id);
  }

  @PostMapping(value = "/events/bars/{barId}", consumes = MediaType.APPLICATION_JSON_VALUE)
  public void addSportEventForBarDirectly(@RequestBody SportEventEntity sportEvent, @PathVariable Integer barId) {
    sporteventsService.addSportEventForBar(sportEvent, barId);
  }

  @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
  public void createSportEvent(@RequestBody SportEventEntity sportEvent) {
    sporteventsService.createSportEvent(sportEvent);
  }

  @GetMapping(value = "/{sportEventId}")
  public SportEventEntity findSportEventById(@PathVariable Integer sportEventId) {
    return sporteventsService.findSportEventById(sportEventId);
  }

  @DeleteMapping(value = "/{sportEventId}")
  public void deleteSportEventById(@PathVariable Integer sportEventId) {
    sporteventsService.deleteSportEventById(sportEventId);
  }
}