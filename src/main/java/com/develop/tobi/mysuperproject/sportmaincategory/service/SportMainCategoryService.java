package com.develop.tobi.mysuperproject.sportmaincategory.service;

import java.util.List;

import com.develop.tobi.mysuperproject.model.SportMainCategoryEntity;

public interface SportMainCategoryService {

  List<SportMainCategoryEntity> findAll();

  SportMainCategoryEntity findById(Integer id);

  SportMainCategoryEntity save(SportMainCategoryEntity sportMainCategoryEntity);

  void deleteMainCategoryById(Integer id);
}
