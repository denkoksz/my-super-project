package com.develop.tobi.mysuperproject.sportmaincategory.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.develop.tobi.mysuperproject.model.SportMainCategoryEntity;

@Repository
public interface SportMainCategoryRepository extends JpaRepository<SportMainCategoryEntity, Integer> {

}
