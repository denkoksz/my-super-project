package com.develop.tobi.mysuperproject.sportmaincategory.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.develop.tobi.mysuperproject.model.SportMainCategoryEntity;
import com.develop.tobi.mysuperproject.sportmaincategory.service.SportMainCategoryService;

@RestController
@RequestMapping("/api/sportmaincategory")
public class SportMainCategoryController {

  private final SportMainCategoryService sportMainCategoryService;

  @Autowired
  public SportMainCategoryController(final SportMainCategoryService sportMainCategoryService) {
    this.sportMainCategoryService = sportMainCategoryService;
  }

  @GetMapping
  public List<SportMainCategoryEntity> getAllMainCategory() {
    return sportMainCategoryService.findAll();
  }

  @GetMapping(value = "/{id}")
  public SportMainCategoryEntity findById(@PathVariable("id") Integer id) {
    return sportMainCategoryService.findById(id);
  }

  @PostMapping
  public SportMainCategoryEntity createNewSportMainCategory(@RequestBody SportMainCategoryEntity sportMainCategoryEntity) {
    return sportMainCategoryService.save(sportMainCategoryEntity);
  }

  @DeleteMapping(value = "/{id}")
  public void deleteMainCategoryById(@PathVariable("id") Integer id) {
    sportMainCategoryService.deleteMainCategoryById(id);
  }

  @PutMapping(value = "/{id}")
  public SportMainCategoryEntity updateMainCategoryById(@RequestBody SportMainCategoryEntity sportMainCategoryEntity, @PathVariable Integer id){
    sportMainCategoryService.findById(id);
    sportMainCategoryEntity.setPkMainCategory(id);
    return sportMainCategoryService.save(sportMainCategoryEntity);
  }
}
