package com.develop.tobi.mysuperproject.sportmaincategory.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.develop.tobi.mysuperproject.model.SportMainCategoryEntity;
import com.develop.tobi.mysuperproject.sportmaincategory.repository.SportMainCategoryRepository;

@Service
public class SportMainCategoryImpl implements SportMainCategoryService{

  private final SportMainCategoryRepository sportMainCategoryRepository;

  @Autowired
  public SportMainCategoryImpl(final SportMainCategoryRepository sportMainCategoryRepository) {
    this.sportMainCategoryRepository = sportMainCategoryRepository;
  }

  @Override
  public List<SportMainCategoryEntity> findAll() {
    return sportMainCategoryRepository.findAll();
  }

  @Override
  public SportMainCategoryEntity findById(final Integer id) {
    return sportMainCategoryRepository.findById(id).orElseThrow(() -> new RuntimeException("Sport Main Category not found with id: " + id));
  }

  @Override
  public SportMainCategoryEntity save(final SportMainCategoryEntity sportMainCategoryEntity) {
    return sportMainCategoryRepository.save(sportMainCategoryEntity);
  }

  @Override
  public void deleteMainCategoryById(final Integer id) {
    sportMainCategoryRepository.deleteById(id);
  }
}
