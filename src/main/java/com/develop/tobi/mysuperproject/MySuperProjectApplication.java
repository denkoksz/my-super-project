package com.develop.tobi.mysuperproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MySuperProjectApplication {

	public static void main(String[] args) {
		System.out.println("starting the app...");
		SpringApplication.run(MySuperProjectApplication.class, args);
	}

}