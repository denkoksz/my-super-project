package com.develop.tobi.mysuperproject.team.repository.custom;

import com.develop.tobi.mysuperproject.model.TeamEntity;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TeamCustomRepository {

    List<TeamEntity> getTeamsByCategoryAndCountryAndLeague(String category, String country, String league);
}
