package com.develop.tobi.mysuperproject.team.repository;

import com.develop.tobi.mysuperproject.model.TeamEntity;
import com.develop.tobi.mysuperproject.team.repository.custom.TeamCustomRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TeamRepository extends TeamCustomRepository, JpaRepository<TeamEntity, Integer> {

}
