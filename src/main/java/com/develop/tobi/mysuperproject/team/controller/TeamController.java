package com.develop.tobi.mysuperproject.team.controller;

import com.develop.tobi.mysuperproject.model.TeamEntity;
import com.develop.tobi.mysuperproject.team.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/team")
public class TeamController {

  private final TeamService teamService;

  @Autowired
  public TeamController(final TeamService teamService) {
    this.teamService = teamService;
  }

  @GetMapping
  public List<TeamEntity> getAllTeam() {
    return teamService.findAll();
  }

  @GetMapping(value = "filter")
  public List<TeamEntity> getTeamsByCategoryAndCountryAndLeague(
          @RequestParam("category") String category,
          @RequestParam("country") String country,
          @RequestParam("league") String league)
  {
    return teamService.getTeamsByCategoryAndCountryAndLeague(category, country, league);
  }

  @GetMapping(value = "/{id}")
  public TeamEntity findById(@PathVariable("id") Integer id) {
    return teamService.findById(id);
  }

  @PostMapping
  public TeamEntity createNewTeamEntity(@RequestBody TeamEntity teamEntity) {
    return teamService.save(teamEntity);
  }

  @DeleteMapping(value = "/{id}")
  public void deleteTeamById(@PathVariable("id") Integer id) {
    teamService.deleteTeamById(id);
  }

  @PutMapping(value = "/{id}")
  public TeamEntity updateTeamEntity(@RequestBody TeamEntity teamEntity, @PathVariable Integer id){
    teamService.findById(id);
    teamEntity.setPkTeam(id);
    return teamService.save(teamEntity);
  }
}
