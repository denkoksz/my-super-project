package com.develop.tobi.mysuperproject.team.service;

import com.develop.tobi.mysuperproject.model.TeamEntity;
import com.develop.tobi.mysuperproject.team.repository.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeamServiceImpl implements TeamService {

  private final TeamRepository teamRepository;

  @Autowired
  public TeamServiceImpl(final TeamRepository teamRepository) {
    this.teamRepository = teamRepository;
  }

  @Override
  public List<TeamEntity> findAll() {
    return teamRepository.findAll();
  }

  @Override
  public List<TeamEntity> getTeamsByCategoryAndCountryAndLeague(String category, String country, String league) {
    return teamRepository.getTeamsByCategoryAndCountryAndLeague(category, country, league);
  }

  @Override
  public TeamEntity findById(final Integer id) {
    return teamRepository.findById(id).orElseThrow(() -> new RuntimeException("Team not found with id: " + id));
  }

  @Override
  public TeamEntity save(final TeamEntity teamEntity) {
    return teamRepository.save(teamEntity);
  }

  @Override
  public void deleteTeamById(final Integer id) {
    teamRepository.deleteById(id);
  }
}
