package com.develop.tobi.mysuperproject.team.service;

import com.develop.tobi.mysuperproject.model.TeamEntity;

import java.util.List;

public interface TeamService {

  List<TeamEntity> findAll();

  List<TeamEntity> getTeamsByCategoryAndCountryAndLeague(String category, String country, String league);

  TeamEntity findById(Integer id);

  TeamEntity save(TeamEntity teamEntity);

  void deleteTeamById(Integer id);
}
