package com.develop.tobi.mysuperproject.team.repository.impl;

import com.develop.tobi.mysuperproject.model.QTeamEntity;
import com.develop.tobi.mysuperproject.model.TeamEntity;
import com.develop.tobi.mysuperproject.team.repository.custom.TeamCustomRepository;
import com.querydsl.jpa.impl.JPAQuery;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class TeamRepositoryImpl implements TeamCustomRepository {

    private static final QTeamEntity TEAM_ENTITY = QTeamEntity.teamEntity;

    @PersistenceContext
    private EntityManager entityManager;


    @Override
    public List<TeamEntity> getTeamsByCategoryAndCountryAndLeague(String category, String country, String league) {
        final JPAQuery<TeamEntity> query = new JPAQuery<>(entityManager);

        query.from(TEAM_ENTITY)
                .where(TEAM_ENTITY.mainCategory.eq(category)
                        .and(TEAM_ENTITY.country.eq(country))
                        .and(TEAM_ENTITY.leagueName.eq(league)));

        return query.select(TEAM_ENTITY).fetch();
    }
}
