package com.develop.tobi.mysuperproject.country.service;

import java.util.List;

import com.develop.tobi.mysuperproject.model.CountryEntity;

public interface CountryService {

  CountryEntity findById(Integer id);

  List<CountryEntity> findAll();

  CountryEntity save(CountryEntity countryEntity);

  void deleteCountryById(Integer id);
  List<CountryEntity> getCountriesByCategory(String category);
}
