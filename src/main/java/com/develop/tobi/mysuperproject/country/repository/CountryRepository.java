package com.develop.tobi.mysuperproject.country.repository;

import com.develop.tobi.mysuperproject.country.repository.custom.CountryCustomRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.develop.tobi.mysuperproject.model.CountryEntity;

@Repository
public interface CountryRepository extends CountryCustomRepository, JpaRepository<CountryEntity, Integer> {

}
