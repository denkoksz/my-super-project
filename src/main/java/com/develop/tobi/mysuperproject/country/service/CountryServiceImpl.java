package com.develop.tobi.mysuperproject.country.service;

import com.develop.tobi.mysuperproject.country.repository.CountryRepository;
import com.develop.tobi.mysuperproject.model.CountryEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CountryServiceImpl implements CountryService {

  private final CountryRepository countryRepository;

  @Autowired
  public CountryServiceImpl(final CountryRepository countryRepository) {
    this.countryRepository = countryRepository;
  }

  @Override
  public CountryEntity findById(final Integer id) {
    return countryRepository.findById(id).orElseThrow(() -> new RuntimeException("Country not found with ID: " + id));
  }

  @Override
  public List<CountryEntity> findAll() {
    return countryRepository.findAll();
  }

  @Override
  public CountryEntity save(final CountryEntity countryEntity) {
    return countryRepository.save(countryEntity);
  }

  @Override
  public void deleteCountryById(final Integer id) {
    countryRepository.deleteById(id);
  }

  @Override
  public List<CountryEntity> getCountriesByCategory(String category) {
    return countryRepository.getCountriesByCategory(category);
  }

}
