package com.develop.tobi.mysuperproject.country.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.develop.tobi.mysuperproject.country.service.CountryService;
import com.develop.tobi.mysuperproject.model.CountryEntity;

@RestController
@RequestMapping("/api/country")
public class CountryController {

  private final CountryService countryService;

  @Autowired
  public CountryController(final CountryService countryService) {
    this.countryService = countryService;
  }

  @GetMapping
  public List<CountryEntity> getAllCountry() {
    return countryService.findAll();
  }

  @GetMapping(value = "/{id}")
  public CountryEntity findById(@PathVariable("id") Integer id) {
    return countryService.findById(id);
  }

  @PostMapping
  public CountryEntity createNewCountry(@RequestBody CountryEntity countryEntity) {
    return countryService.save(countryEntity);
  }

  @DeleteMapping(value = "/{id}")
  public void deleteCountryById(@PathVariable("id") Integer id) {
    countryService.deleteCountryById(id);
  }

  @PutMapping(value = "/{id}")
  public CountryEntity updateCountryById(@RequestBody CountryEntity countryEntity, @PathVariable Integer id){
    countryService.findById(id);
    countryEntity.setPkCountry(id);
    return countryService.save(countryEntity);
  }

  @GetMapping(value = "filter")
  public List<CountryEntity> getCountriesByCategory(@RequestParam("category") String category) {
    return countryService.getCountriesByCategory(category);
  }
}
