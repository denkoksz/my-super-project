package com.develop.tobi.mysuperproject.country.repository.custom;

import com.develop.tobi.mysuperproject.model.CountryEntity;

import java.util.List;

public interface CountryCustomRepository {

        List<CountryEntity> getCountriesByCategory(String category);
}
