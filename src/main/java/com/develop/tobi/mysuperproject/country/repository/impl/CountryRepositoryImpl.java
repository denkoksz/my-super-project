package com.develop.tobi.mysuperproject.country.repository.impl;

import com.develop.tobi.mysuperproject.country.repository.custom.CountryCustomRepository;
import com.develop.tobi.mysuperproject.model.*;
import com.querydsl.jpa.impl.JPAQuery;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class CountryRepositoryImpl implements CountryCustomRepository {

    private static final QCountryEntity COUNTRY_ENTITY = QCountryEntity.countryEntity;
    private static final QLeaguesEntity LEAGUES_ENTITY = QLeaguesEntity.leaguesEntity;

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<CountryEntity> getCountriesByCategory(String category) {
        final JPAQuery<BarsEntity> query = new JPAQuery<>(entityManager);

        query.from(COUNTRY_ENTITY)
                .join(LEAGUES_ENTITY).on(COUNTRY_ENTITY.countryName.eq(LEAGUES_ENTITY.country))
                .where(LEAGUES_ENTITY.mainCategory.eq(category));

        return query.select(COUNTRY_ENTITY).distinct().fetch();
    }
}
