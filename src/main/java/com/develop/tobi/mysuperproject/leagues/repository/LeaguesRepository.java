package com.develop.tobi.mysuperproject.leagues.repository;

import com.develop.tobi.mysuperproject.leagues.repository.custom.LeaguesCustomRepository;
import com.develop.tobi.mysuperproject.model.LeaguesEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LeaguesRepository extends LeaguesCustomRepository, JpaRepository<LeaguesEntity, Integer> {
}
