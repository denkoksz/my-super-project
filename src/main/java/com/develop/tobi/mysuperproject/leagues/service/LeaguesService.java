package com.develop.tobi.mysuperproject.leagues.service;

import com.develop.tobi.mysuperproject.model.LeaguesEntity;

import java.util.List;

public interface LeaguesService {

    List<LeaguesEntity> getLeagues();

    List<LeaguesEntity> getLeaguesByCategoryAndCountry(String category, String country);
}
