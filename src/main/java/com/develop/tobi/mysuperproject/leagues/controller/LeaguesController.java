package com.develop.tobi.mysuperproject.leagues.controller;

import com.develop.tobi.mysuperproject.leagues.service.LeaguesService;
import com.develop.tobi.mysuperproject.model.LeaguesEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/leagues")
public class LeaguesController {

    private final LeaguesService leaguesService;

    @Autowired
    public LeaguesController(LeaguesService leaguesService) {
        this.leaguesService = leaguesService;
    }

    @GetMapping
    public List<LeaguesEntity> getAllLeagues() {
        return leaguesService.getLeagues();
    }

    @GetMapping(value = "filter")
    public List<LeaguesEntity> getLeaguesByCategoryAndCountry(@RequestParam("category") String category, @RequestParam("country") String country) {
        return leaguesService.getLeaguesByCategoryAndCountry(category, country);
    }

}
