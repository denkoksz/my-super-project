package com.develop.tobi.mysuperproject.leagues.service;

import com.develop.tobi.mysuperproject.leagues.repository.LeaguesRepository;
import com.develop.tobi.mysuperproject.model.LeaguesEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LeaguesServiceImpl implements LeaguesService {

    private final LeaguesRepository leaguesRepository;

    public LeaguesServiceImpl(final LeaguesRepository leaguesRepository) {
        this.leaguesRepository = leaguesRepository;
    }

    @Override
    public List<LeaguesEntity> getLeagues() {
        return leaguesRepository.findAll();
    }

    @Override
    public List<LeaguesEntity> getLeaguesByCategoryAndCountry(String category, String country) {
        return leaguesRepository.getLeaguesByCategoryAndCountry(category, country);
    }
}
