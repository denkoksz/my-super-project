package com.develop.tobi.mysuperproject.leagues.repository.custom;

import com.develop.tobi.mysuperproject.model.LeaguesEntity;

import java.util.List;

public interface LeaguesCustomRepository {

    List<LeaguesEntity> getLeagues();

    List<LeaguesEntity> getLeaguesByCategoryAndCountry(String category, String country);
}
