package com.develop.tobi.mysuperproject.leagues.repository.impl;

import com.develop.tobi.mysuperproject.leagues.repository.custom.LeaguesCustomRepository;
import com.develop.tobi.mysuperproject.model.BarsEntity;
import com.develop.tobi.mysuperproject.model.LeaguesEntity;
import com.develop.tobi.mysuperproject.model.QLeaguesEntity;
import com.develop.tobi.mysuperproject.model.SportEventEntity;
import com.querydsl.jpa.impl.JPAQuery;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class LeaguesRepositoryImpl implements LeaguesCustomRepository {

    private static final QLeaguesEntity LEAGUES_ENTITY = QLeaguesEntity.leaguesEntity;

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<LeaguesEntity> getLeagues() {
        final JPAQuery<SportEventEntity> query = new JPAQuery<>(entityManager);

        query.from(LEAGUES_ENTITY);
        return query.select(LEAGUES_ENTITY).fetch();
    }

    @Override
    public List<LeaguesEntity> getLeaguesByCategoryAndCountry(String category, String country) {
        final JPAQuery<BarsEntity> query = new JPAQuery<>(entityManager);

        query.from(LEAGUES_ENTITY)
                .where(LEAGUES_ENTITY.mainCategory.eq(category)
                        .and(LEAGUES_ENTITY.country.eq(country)));

        return query.select(LEAGUES_ENTITY).fetch();
    }
}
