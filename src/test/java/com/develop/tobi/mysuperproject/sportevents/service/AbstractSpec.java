package com.develop.tobi.mysuperproject.sportevents.service;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.develop.tobi.mysuperproject.MSPProfile;
import com.develop.tobi.mysuperproject.MySuperProjectApplication;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = MySuperProjectApplication.class, webEnvironment = RANDOM_PORT)
@ActiveProfiles(profiles = {MSPProfile.UNITTEST})
public abstract class AbstractSpec {

}
