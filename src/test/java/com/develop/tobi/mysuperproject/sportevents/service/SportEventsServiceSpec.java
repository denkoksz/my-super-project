package com.develop.tobi.mysuperproject.sportevents.service;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.develop.tobi.mysuperproject.bars.service.BarsService;
import com.develop.tobi.mysuperproject.model.BarsEntity;
import com.develop.tobi.mysuperproject.model.SportEventEntity;

import static org.junit.Assert.*;

public class SportEventsServiceSpec extends AbstractServiceSpec {

  @Autowired
  private SportEventsService sporteventsService;

  @Autowired
  private BarsService barsService;

  private final SportEventEntity se1 = new SportEventEntity("labdarúgás", "Nemzetközi",
    "Bajnokok Ligája", "FC Barcelona", "Juventus FC", LocalDateTime.now().plusDays(1));

  private final SportEventEntity se2 = new SportEventEntity("kézilabda", "Nemzetközi",
    "Bajnokok Ligája", "FC Barcelona", "Juventus FC", LocalDateTime.now().plusDays(3));

  private final SportEventEntity se3 = new SportEventEntity("jéghoki", "Magyarország",
    "Erste Liga", "Ferencváros", "Újpest", LocalDateTime.now().plusDays(5));

  private final BarsEntity bar1 = new BarsEntity("HU", "Test Bar", "1119", "Budapest", "Test utca",
    "10", "0000", " ", 1, 0, "http://");

  @Test
  public void getAllSportEvents() {
    sporteventsService.save(se1);
    List<SportEventEntity> sportEventEntities = sporteventsService.findAll();
    SportEventEntity testSe1 = sportEventEntities.get(sportEventEntities.size() - 1);

    assertEquals(testSe1.getPkEvent(), se1.getPkEvent());
  }

  @Test
  public void getSportEventsInNextXDays() {
    saveAndFlushMoreEntities(se1, se2);

    List<SportEventEntity> eventsInNext2Days = sporteventsService.getSportEventsInNextXDays(2);

    assertEquals(1, eventsInNext2Days.size());
  }

  @Test
  public void getSportEventsByBarId() {
    saveAndFlushMoreEntities(se1, se2);
    barsService.save(bar1);
    sporteventsService.addSportEventForBar(se1, bar1.getPkBar());

    List<SportEventEntity> allSportEventsByBarId = sporteventsService.getSportEventsByBarId(bar1.getPkBar());

    assertEquals(1, allSportEventsByBarId.size());
    assertEquals("Juventus FC", allSportEventsByBarId.get(0).getAwayTeamName());
  }

  @Test
  public void getBarsBySportEventId() {
    sporteventsService.save(se1);
    barsService.save(bar1);
    sporteventsService.addSportEventForBar(se1, bar1.getPkBar());

    List<BarsEntity> allBarsBySportEventId = sporteventsService.getBarsBySportEventId(se1.getPkEvent());

    assertEquals(1, allBarsBySportEventId.size());
    assertEquals("10", allBarsBySportEventId.get(0).getHouseNumber());
  }

  @Test
  public void createSameSportEventTwice() {
    sporteventsService.createSportEvent(se1);

    assertThrows(RuntimeException.class, () -> sporteventsService.createSportEvent(se1));
  }

  @Test
  public void createSportEventForBarSuccessful() {
    sporteventsService.createSportEvent(se1);
    barsService.save(bar1);
    sporteventsService.addSportEventForBar(se2, bar1.getPkBar());
    assertEquals(2, sporteventsService.findAll().size());
  }

  @Test
  public void findSportEvent() {
    SportEventEntity expectedSportEvent = new SportEventEntity("labdarúgás", "Nemzetközi",
            "Bajnokok Ligája", "FC Barcelona", "Juventus FC", LocalDateTime.now().plusDays(1));
    sporteventsService.createSportEvent(se1);
    SportEventEntity resultSportEvent = sporteventsService.findSportEvent(expectedSportEvent);

    assertEquals(expectedSportEvent.getHomeTeamName(), resultSportEvent.getHomeTeamName());
    assertEquals(expectedSportEvent.getAwayTeamName(), resultSportEvent.getAwayTeamName());
    assertEquals(expectedSportEvent.getMainCategory(), resultSportEvent.getMainCategory());
    assertEquals(expectedSportEvent.getEventDate().getDayOfYear(), resultSportEvent.getEventDate().getDayOfYear());
  }

  @Test
  public void cannotFindSportEvent_EventDateNotInRange() {
    SportEventEntity sameEntityButAnotherEventTime = new SportEventEntity("labdarúgás", "Nemzetközi",
            "Bajnokok Ligája", "FC Barcelona", "Juventus FC", LocalDateTime.now().plusDays(2));
    sporteventsService.createSportEvent(se1);
    SportEventEntity resultSportEvent = sporteventsService.findSportEvent(sameEntityButAnotherEventTime);

    assertNull(resultSportEvent);
  }



  private void saveAndFlushMoreEntities(SportEventEntity... entities) {
    Arrays.stream(entities).
      forEach(e -> sporteventsService.save(e));
  }
}